import React, { Component } from "react";
import like from "../../../assets/images/like.png"
class LikeImage extends Component {
    render() {
        let { outputMessageProps, likeDisplayProps } = this.props;

        return (
            <React.Fragment>
                <div className="row mt-3">
                {outputMessageProps.map((element, index) => {
                    return <p key={index}>{element}</p>
                })}
                
                    <p></p>
                    { likeDisplayProps ? <img src={like} alt="like" style={{ width: "100px", margin: "0 auto" }}></img> : <></> }
                </div>
            </React.Fragment>
        )
    }
}

export default LikeImage