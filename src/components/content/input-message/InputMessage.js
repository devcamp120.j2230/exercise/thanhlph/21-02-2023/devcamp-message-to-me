import React, { Component } from "react";

class InputMessage extends Component {
    onInputChangeHandler = (event) => {
        let value = event.target.value;

        console.log("Input đang được nhập");
        console.log(value);

        this.props.inputMessageChangeHandlerProps(value);
    }

    onButtonClickHandler = () => {
        this.props.outputMessageChangeHandlerProps();
    }

    render() {
        return (
            <>
                <div className="row mt-3">
                    <label className="form-label">Message cho bạn 12 tháng tới: </label>
                    <input className="form-control" onChange={this.onInputChangeHandler} value={this.props.inputMessageProps}/>
                </div>
                <div className="row mt-3">
                    <button className="btn btn-success" onClick={this.onButtonClickHandler}>Gửi thông điệp</button>
                </div>
            </>
        )
    }
}

export default InputMessage