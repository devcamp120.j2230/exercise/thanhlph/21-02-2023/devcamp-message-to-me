import { Component } from "react";
import background from "../../../assets/images/background.jpg"

class TitleImage extends Component {
    render() {
        return (
            <div className="row mt-3">
                <img src={background} alt="background" width="500px"></img>
            </div>
        )
    }
}
export default TitleImage;